import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;

import static org.junit.Assert.assertEquals;

/**
 * Created by SMihai on 5/23/2017.
 */
public class TestClass {

    private static Logger log = LogManager.getLogger(TestClass.class);

    @BeforeAll
    static void init(){
        log.info("Initialize resources");
    }

    @Test
    @DisplayName("A simple JUnit test")
    public void TestMethod(){
        log.info("JUnit test");
        assertEquals(5,5);
    }

    @AfterAll
    static void cleanup(){
        log.info("Cleanup");
    }
}
